# IRWS_GroupAssignment
Information Retrieval Group Project Using Lucene

## Go to application folder

```cd irws_groupassignment_group1```

# To run the application, preferred Analyzer and Similarity should be passed as System Arguments

## Analyzer with Best Results is CustomAnalyzer

## Similarity with Best Results is BM25Similarity

### Execute below command to get the results

```java -jar target/CA-2-0.0.1-SNAPSHOT.jar CustomAnalyzer BM25Similarity```

## Other Supported Analyzers [EnglishAnalyzer, StandardAnalyzer, WhitespaceAnalyzer]

## Other Supported Similarity [Classic, LMDirichletSimilarity]

```java -jar target/CA-2-0.0.1-SNAPSHOT.jar <Analyzer> <Similarity>```

### For Example
```java -jar target/CA-2-0.0.1-SNAPSHOT.jar CustomAnalyzer LMDirichletSimilarity```

### OR
```java -jar target/CA-2-0.0.1-SNAPSHOT.jar EnglishAnalyzer BM25Similarity```


# For evaluation using trec_eval

## Go to trec_eval folder

```cd trec_eval-9.0.7/```

## Execute the trec_eval

```./trec_eval <QRelFile> ../SearchResult/searching_<Analyzer>_<Similarity>/Results.out```

### For Example

```./trec_eval <QRelFile> ../SearchResult/searching_customAnalyzer_BM25Similarity/Results.out```