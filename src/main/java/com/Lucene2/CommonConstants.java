package com.Lucene2;

/**
 * @author aishwaryaagarwal
 *
 */
public class CommonConstants {

	// File with common configuration values
	public static final String VALUES_FILE = "src/main/resources/commonResources.properties";

	public static final String ARTICLES_DATA_FOLDER = "lucene.articles.input.folderpath";
	public static final String QUERY_FILE = "lucene.topics.query.filepath";
	public static final String INDEX_FOLDER = "lucene.indexes.folderpath";
	public static final String SEARCH_RESULT_FOLDER = "lucene.search.results.folderpath";
	public static final String NEWS_RESOURCE_NAMES = "lucene.news.resource.names";
	public static final String QUERY_CHOICE = "lucene.search.strategy.choice";
}
