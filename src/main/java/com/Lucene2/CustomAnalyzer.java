package com.Lucene2;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.en.EnglishMinimalStemFilter;
import org.apache.lucene.analysis.en.KStemFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterGraphFilter;
import org.apache.lucene.analysis.snowball.SnowballFilter;
import org.apache.lucene.analysis.standard.ClassicTokenizer;
import org.apache.lucene.analysis.StopFilter;

/**
 * @author aishwaryaagarwal
 *
 */
public class CustomAnalyzer extends Analyzer {

	@Override
	protected TokenStreamComponents createComponents(String s) {
		//Tokenize 
		Tokenizer source = new ClassicTokenizer();
		
		//Add different filters to token stream
		TokenStream tokenStream = new LowerCaseFilter(source);
		tokenStream = new EnglishMinimalStemFilter(tokenStream);
		tokenStream = new KStemFilter(tokenStream);
		tokenStream = new PorterStemFilter(tokenStream);
		tokenStream = new WordDelimiterGraphFilter(tokenStream,
                WordDelimiterGraphFilter.GENERATE_WORD_PARTS
              | WordDelimiterGraphFilter.STEM_ENGLISH_POSSESSIVE, null);
		tokenStream = new SnowballFilter(tokenStream, "English");
		String[] stop_words = StopWords.getEnglishStopWordList();
		tokenStream = new StopFilter(tokenStream, StopFilter.makeStopSet(stop_words, true));
		return new TokenStreamComponents(source, tokenStream);
	}

}
