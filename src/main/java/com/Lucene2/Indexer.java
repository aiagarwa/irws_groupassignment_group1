package com.Lucene2;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Indexer {
	private static ArrayList<HashMap<String, String>> parsedFile;
	private static Analyzer analyzer;
	private static Similarity similarity;
	private static String indexFolderPath;

	/**
	 * @param analyzer
	 * @param similarity
	 * @param newspaper
	 * @param indexFolderPath
	 */
	public Indexer(Analyzer analyzer, Similarity similarity, ArrayList<HashMap<String, String>> parsedFile,
			String indexFolderPath) {
		// TODO Auto-generated constructor stub
		this.parsedFile = parsedFile;
		this.analyzer = analyzer;
		this.similarity = similarity;
		this.indexFolderPath = indexFolderPath;
	}

	/**
	 * Method to Index File
	 * 
	 * @throws Exception
	 */
	public void indexing() throws Exception {

		// To store an index on Disk
		Directory indexFolder = FSDirectory.open(Paths.get(indexFolderPath));

		// Set analyzer and Similarity for Indexing
		IndexWriterConfig wriConf = new IndexWriterConfig(analyzer);
		wriConf.setSimilarity(similarity);

		// Index Opening Mode
		wriConf.setOpenMode(OpenMode.CREATE_OR_APPEND);
		IndexWriter writer = new IndexWriter(indexFolder, wriConf);

		// Read parsed file and Create a new document
		for (HashMap<String, String> singleDoc : parsedFile) {
			Document doc = new Document();
			doc.add(new StringField("docId", singleDoc.get("docId"), Field.Store.YES));
			doc.add(new TextField("headline", singleDoc.get("title"), Field.Store.YES));
			doc.add(new TextField("text", singleDoc.get("content"), Field.Store.YES));
			doc.add(new TextField("date", singleDoc.get("date"), Field.Store.YES));

			// Save the document to the index
			writer.addDocument(doc);
		}
		// Commit changes and close everything
		writer.close();
		indexFolder.close();

	}
}
