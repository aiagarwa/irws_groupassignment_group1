package com.Lucene2;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.Similarity;

public class MainLucene {

	public static void main(String[] args) throws Exception {

		// Load properties file
		Properties config = CommonUtils.loadPropertyFile(CommonConstants.VALUES_FILE);

		// Get all the newspaper resources
		String[] newspapers = CommonUtils.getKeyValues(CommonConstants.VALUES_FILE,
				CommonConstants.NEWS_RESOURCE_NAMES);

		// Set Analyzer and Similarity based on the system arguments
		Analyzer analyzer = CommonUtils.setAnalyzerType(args[0]);
		Similarity similarity = CommonUtils.setSimilarityType(args[1]);

		// Create Index folder if does not exist
		CommonUtils.checkAndCreateFolder(config.getProperty(CommonConstants.INDEX_FOLDER));

		String indexFolder = config.getProperty(CommonConstants.INDEX_FOLDER) + "/indexing_" + args[0] + "_" + args[1];

		if (!CommonUtils.checkIfFolderExists(indexFolder)) {
			for (String newspaper : newspapers) {
				// Parse the newspaper articles files
				System.out.println("Parsing: " + newspaper);
				ArrayList<HashMap<String, String>> parsedFile = ParseFiles
						.parseIndexFile(config.getProperty(CommonConstants.ARTICLES_DATA_FOLDER) + newspaper);

				// Indexing
				System.out.println("Indexing: " + newspaper);
				Indexer ind = new Indexer(analyzer, similarity, parsedFile, indexFolder);
				ind.indexing();
				System.out.println("Indexing for " + newspaper + " completed");
			}
		}
		System.out.println("Indexing Completed.....");
		// Create Results folder
		CommonUtils.checkAndCreateFolder(config.getProperty(CommonConstants.SEARCH_RESULT_FOLDER));

		System.out.print("Query Search Starts.....");

		// Parse the Query Topics file
		ArrayList<HashMap<String, String>> parsedQueryFile = ParseFiles
				.parseTopic(config.getProperty(CommonConstants.QUERY_FILE));

		Query query = new Query(analyzer, similarity,
				config.getProperty(CommonConstants.SEARCH_RESULT_FOLDER) + "/searching_" + args[0] + "_" + args[1],
				Paths.get(indexFolder), parsedQueryFile, config.getProperty(CommonConstants.QUERY_CHOICE));
		query.queryHandler();

	}
}