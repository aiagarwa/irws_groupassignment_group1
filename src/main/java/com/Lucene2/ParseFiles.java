/**
 * 
 */
package com.Lucene2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author aishwaryaagarwal
 *
 */
public class ParseFiles {

	/**
	 * Method to parse Input(Newspaper Documents) files
	 * @param filepath
	 * @return Parsed Content in ArrayList of HashMap
	 * @throws IOException
	 */
	public static ArrayList<HashMap<String, String>> parseIndexFile(String filepath) throws IOException {
		ArrayList<HashMap<String, String>> compDoc = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> singleDoc = new HashMap<String, String>();

		String files_location = filepath;
		File news_directory = new File(files_location);
		File[] docs_list = news_directory.listFiles();
		File[] files;
		for (File sub : docs_list) {
			if (sub.isDirectory()) {
				files = sub.listFiles();
				for (File file : files) {
					org.jsoup.nodes.Document parsed_d = Jsoup.parse(file, "UTF-8", "");
					Elements documents = parsed_d.select("doc");

					for (Element document : documents) {
						singleDoc = new HashMap<String, String>();
						singleDoc.put("docId", document.select("docno").text());
						singleDoc.put("content",
								document.select("text").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase()));
						singleDoc.put("title",
								document.select("ti").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase())
										+ document.select("headline").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]",
												"".toLowerCase()));
						singleDoc.put("date",
								document.select("date1").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase())
										+ document.select("date").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]",
												"".toLowerCase()));
						compDoc.add(singleDoc);
					}
				}
			} else {
				org.jsoup.nodes.Document parsed_d = Jsoup.parse(sub, "UTF-8", "");
				Elements documents = parsed_d.select("doc");

				for (Element document : documents) {
					singleDoc = new HashMap<String, String>();
					singleDoc.put("docId", document.select("docno").text());
					singleDoc.put("content",
							document.select("text").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase()));
					singleDoc.put("title",
							document.select("ti").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase())
									+ document.select("headline").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]",
											"".toLowerCase()));
					singleDoc.put("date",
							document.select("date1").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase())
									+ document.select("date").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]",
											"".toLowerCase()));
					compDoc.add(singleDoc);
				}
			}
		}
		System.out.println("Total Documents parsed : " + compDoc.size());
		return compDoc;
	}

	/**
	 * Method to parse Search Topics
	 * @param filepath
	 * @return Parsed Content in ArrayList of HashMap
	 * @throws IOException
	 */
	public static ArrayList<HashMap<String, String>> parseTopic(String filepath) throws IOException {
		ArrayList<HashMap<String, String>> compDoc = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> singleDoc = new HashMap<String, String>();

		String files_location = filepath;
		File searchTopics = new File(files_location);
		org.jsoup.nodes.Document parsed_d = Jsoup.parse(searchTopics, "UTF-8", "");
		Elements documents = parsed_d.select("top");
		for (Element document : documents) {
			singleDoc = new HashMap<String, String>();
			singleDoc.put("num", document.select("num").text().substring(8, 11));
			singleDoc.put("title",
					document.select("title").text().replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase()));
			String description = document.select("desc").text();
			singleDoc.put("desc", description.substring(description.indexOf(" "), description.indexOf("Narrative"))
					.replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase()));
		
			String[] temp = description.substring(description.indexOf("Narrative")).split("\\.");
 			String narrative = "";
			for(String t: temp)
			{
				if(t.contains(";")) {
					String[] temp2 = t.split("\\;");
					for(String t2: temp2)
					{
						if(!t2.contains("not relevant"))
							narrative=narrative+t2;
					}
				}
				else
				if(!t.contains("not relevant"))
					narrative=narrative+t;
			}
			singleDoc.put("narrative",
					narrative.replaceAll("[^a-zA-Z0-9,?\\\";. -]", "".toLowerCase()));
			compDoc.add(singleDoc);
		}
		System.out.println("Total Documents parsed : " + compDoc.size());
		return compDoc;
	}

}
