package com.Lucene2;

import java.io.IOException;
import java.nio.file.Path;

import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.similarities.Similarity;

import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.ScoreDoc;
import java.io.FileWriter;
import org.apache.lucene.document.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author aishwaryaagarwal
 *
 */
public class Query {
	private static Analyzer analyzer;
	private static Similarity similarity;
	private static Path indexDir;
	private static String searchResultFolderPath;
	private static ArrayList<HashMap<String, String>> queryTermsList;
	private static String queryChoice;

	/**
	 * @param analyzer
	 * @param similarity
	 * @param searchResultFolderPath
	 * @param indexDir
	 * @param queryTermsList
	 * @param queryChoice
	 */
	public Query(Analyzer analyzer, Similarity similarity, String searchResultFolderPath, Path indexDir,
			ArrayList<HashMap<String, String>> queryTermsList, String queryChoice) {
		this.analyzer = analyzer;
		this.similarity = similarity;
		this.indexDir = indexDir;
		this.searchResultFolderPath = searchResultFolderPath;
		this.queryTermsList = queryTermsList;
		this.queryChoice = queryChoice;
	}

	/**
	 * Query and Search Indexed Files
	 * @throws IOException
	 * @throws ParseException
	 */
	public static void queryHandler() throws IOException, ParseException {

		// Delete folder if existing
		CommonUtils.deleteFolder(searchResultFolderPath);
		// Create Results folder
		CommonUtils.checkAndCreateFolder(searchResultFolderPath);
		
		// Create the Results File
		FileWriter fileWriter = new FileWriter(searchResultFolderPath + "/Results.out");

		// Open the folder that contains our search index
		Directory dir = FSDirectory.open(indexDir);
		
		// create objects to read and search across the index
		IndexReader reader = DirectoryReader.open(dir);
		IndexSearcher searcher = new IndexSearcher(reader);
		
		// Set similarity(Scoring Function)
		searcher.setSimilarity(similarity);

		QueryParser parser = null;
		org.apache.lucene.search.Query query = null;

		//Create and insert values in the hash map for boost
		Map<String, Float> boost = new HashMap<>();
		boost.put("headline", (float) 0.1);
		boost.put("date", (float) 0.1);
		boost.put("text", (float) 0.8);
		

		for (HashMap<String, String> queryTerms : queryTermsList) {
			// Decide how to build query, we can evaluate which one provides the best
			// results
			if (Integer.parseInt(queryChoice) == 1) {
				parser = new MultiFieldQueryParser(new String[] { "headline", "text" },analyzer, boost);
				query = parser.parse(queryTerms.get("title")); // use title only
			} else if (Integer.parseInt(queryChoice) == 2) {
				parser = new MultiFieldQueryParser(new String[] { "headline", "text", "date" }, analyzer, boost);
				query = parser.parse(QueryParser.escape(queryTerms.get("desc"))); // use description only
			} else {
				parser = new MultiFieldQueryParser(new String[] { "headline", "text","date" }, analyzer, boost);
				query = parser.parse(QueryParser.escape(queryTerms.get("title") + " " + queryTerms.get("desc")+" "+queryTerms.get("narrative")));
			}

			// Get the search results
			try {
				TopDocs results = searcher.search(query, 1000);
				int numHits = Integer.parseInt(results.totalHits.toString().replaceAll("[\\D]", ""));

				ScoreDoc[] hits = results.scoreDocs;

				System.out.print("------ New query using topic: " + queryTerms.get("num") + " ------\n==> ");
				System.out.println(numHits + " total matching documents.");

				//Write results into the file
				for (int j = 0; j < hits.length; j++) {
					Document doc = searcher.doc(hits[j].doc);
					String documentId = doc.get("docId");

					if (documentId != null) {
						fileWriter.write(queryTerms.get("num") + " Q0 " + documentId + " " + (j + 1) + " "
								+ hits[j].score + " " + "STANDARD\n");
					}
				}
			} catch (Exception e) {
				System.out.println("> This query has no results");
			}
		}
		fileWriter.close();
		System.out.println("-> Finished processing queries. \nThe top 1000 results for each topic have been logged in "
				+ searchResultFolderPath);

	}
}